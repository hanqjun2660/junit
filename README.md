# JUnit tests
TDD(Test-Driven-Development) 방식의 개발을 숙달하기 위해 JUnit을 사용하여, 테스트 코드를 작성해보고자 함.<br>
개발과 검증의 효율을 올리고 오류 및 버그를 실제 코드에 반영하기 전 찾아내어 디버깅 시간을 줄여,<br>
향상성을 높이고자 JUnit 테스트 프로젝트로 TDD 방식에 익숙해지고자 함.<br>

## 개발 환경
~~~
Java 11
Spring Boot 2.7.11
Junit5
~~~