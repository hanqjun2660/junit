package com.example.junitcalc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JUnitCalcApplication {

    public static void main(String[] args) {
        SpringApplication.run(JUnitCalcApplication.class, args);
    }

}
