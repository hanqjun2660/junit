package com.example.junitcalc;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

// Spring Boot에 포함된 JUnit5로 테스트 클래스를 만들어 테스트 코드를 작성해보았다.
// 아래는 자주 사용하는 어노테이션에 대한 설명을 기재하였다.
// JUnit5 공식문서 -> https://junit.org/junit5/docs/current/user-guide/

/*
@Test : 테스트 메서드임을 나타냄
@ParameterizedTest : 파라미터 테스트를 할 수 있도록 도와주는 어노테이션, @ValueSource를 통해 매개변수를
                     지정할 수 있음.
@RepeatedTest : 반복된 테스트
@TestMethodOrder : 테스트 순위지정. @Order 어노테이션을 통해 우선순위 지정 가능
@DisplayName : 테스트 이름을 지정할 수 있는 어노테이션
@BeforeEach, @AfterEach : 테스트 메서드 시작 전,후 실행하는 어노테이션
@Disabled : 테스트에서 제외
@Timeout : 실행 시간 제한 -> 시간초과시 TimeOutException 발생
*/

@SpringBootTest
class JUnitCalcApplicationTests {

    private static Calculator calculator;

    @BeforeAll
    public static void setUp() {
        calculator = new Calculator();
        System.out.println("계산기 실행 최초 1회");
    }

    @AfterAll
    public static void after() {
        System.out.println("계산기 실행 종료");
    }

    // 더하기 테스트코드 작성
    // given(준비), when(살행), then(검증) 세 영역으로 나누어 준비하였다.
    @Test
    @DisplayName("단일 더하기")
    public void plus() {
        // given -> 사용할 계산기 클래스 준비(재사용을 위해 필드로 빼냄)

        // when -> 계산기 클래스로 인자를 던져준다.
        int result = calculator.plus(1,2);

        // then -> 검증
        assertEquals(3, result);
        System.out.println("단일 더하기 테스트 통과");
    }

    @Test
    @DisplayName("여러개 숫자 더하기")
    public void plusMultiple() {
        int result = calculator.plus(2, 5, 7, 10);
        assertEquals(24, result);
        System.out.println("여러 숫자 더하기 테스트 통과");
    }

}
